//
//  ViewController.swift
//  dz17
//
//  Created by Даниил Карпитский on 2/15/22.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let color = UIColor.init(red: 40, green: 30, blue: 40, alpha:1 )
        view.backgroundColor = color
        // Do any additional setup after loading the view.
    }
    
   
    


}

extension ViewController: UITableViewDataSource {
    
    //amount of sections
//    func numberOfSections(in tableView: UITableView) -> Int{
//        return 2
//    }
    
    
    // amount of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1000
    }
    
    // cells generating
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "firstCell") as! TableViewCell
        
        let red = Float.random(in: 0...1)
        let green = Float.random(in: 0...1)
        let blue = Float.random(in: 0...1)
        let color = UIColor(red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha:1 )
        cell.cellView.backgroundColor = color
        
        cell.cellLabel.text = "red:\(String(format: "%.3f", red)), green:\(String(format: "%.3f", green)), blue:\(String(format: "%.3f", blue))"
        
        return cell
    }
    
    
}
